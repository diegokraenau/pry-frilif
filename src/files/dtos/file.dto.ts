import { IsOptional } from 'class-validator';

export class FileDto {
  @IsOptional()
  name: string;

  @IsOptional()
  link: string;

  @IsOptional()
  category: string;
}
