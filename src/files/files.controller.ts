import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { FileDto } from './dtos';
import { FilesService } from './files.service';

@Controller('files')
export class FilesController {
  constructor(private fileService: FilesService) {}

  @Get()
  async getFiles() {
    const data = await this.fileService.findAll();
    return data;
  }

  @Post()
  async registerFile(@Body() dto: FileDto) {
    const data = await this.fileService.createFile(dto);
    return data;
  }

  @Delete(':id')
  async deleteFile(@Param('id') id: string) {
    const data = await this.fileService.deleteFile(id);
    return data;
  }

  @Put(':id')
  async updateFile(@Param('id') id: string, @Body() dto: FileDto) {
    const data = await this.fileService.updateFile(id, dto);
    return data;
  }
}
