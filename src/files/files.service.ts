import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FileDto } from './dtos';
import { File, FileDocument } from './entities/file.entity';

@Injectable()
export class FilesService {
  constructor(@InjectModel(File.name) private fileModel: Model<FileDocument>) {}

  async findAll() {
    return this.fileModel.find().exec();
  }

  async createFile(dto: FileDto) {
    const file = new this.fileModel(dto);
    return await file.save();
  }

  async deleteFile(id: string) {
    const deleted = await this.fileModel.remove({ _id: id });
    return deleted;
  }

  async updateFile(id: string, dto: FileDto) {
    const updated = await this.fileModel.updateOne({ id }, dto);
    return updated;
  }
}
