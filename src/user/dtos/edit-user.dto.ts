import { IsOptional } from 'class-validator';

export class EditUserDto {
  @IsOptional()
  name: string;

  @IsOptional()
  lastName: string;

  @IsOptional()
  email: string;

  @IsOptional()
  password: string;

  @IsOptional()
  userType: string;

  @IsOptional()
  status: boolean;
}
