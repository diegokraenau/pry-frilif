import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  lastName: string;

  @Prop({ required: true })
  email: string;

  @Prop({default: 'CUSTOMER'})
  userType: string;

  @Prop({ required: true })
  password: string;

  @Prop()
  userName: string;

  @Prop()
  phone: string;

  @Prop({default: true})
  status: boolean;
}

export const UserSchema = SchemaFactory.createForClass(User);
