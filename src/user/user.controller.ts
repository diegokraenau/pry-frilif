import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { EditUserDto, UserDto } from './dtos';
import { UserService } from './user.service';
import { LoginDto } from './dtos/login.dto';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  async getUsers() {
    const data = await this.userService.findAll();
    return data;
  }

  @Post()
  async registerUser(@Body() dto: UserDto) {
    const data = await this.userService.createUser(dto);
    return data;
  }

  @Post('login')
  async login(@Body() dto: LoginDto) {
    const data = await this.userService.login(dto);
    return data;
  }

  @Put(':id')
  async editUser(@Param('id') id: string, @Body() dto: EditUserDto) {
    console.log('aeaeaeaea')
    const data = await this.userService.updateUser(id, dto);
    return data;
  }

  @Get(':id')
  async getUser(@Param('id') id: string) {
    const data = await this.userService.findUserById(id);
    return data;
  }
}
